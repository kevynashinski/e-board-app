package com.samapp.e_board;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.FloatRange;
import android.support.annotation.Nullable;
import android.view.View;

import com.pixplicity.easyprefs.library.Prefs;

import agency.tango.materialintroscreen.MaterialIntroActivity;
import agency.tango.materialintroscreen.SlideFragmentBuilder;
import agency.tango.materialintroscreen.animations.IViewTranslation;

import static com.samapp.e_board.AppConfig.FIRST_RUN;

public class IntroActivity extends MaterialIntroActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!Prefs.getBoolean(FIRST_RUN, true)) {
//            show tutorial
            startActivity(new Intent(IntroActivity.this, SplashScreenActivity.class));
            finish();
        }

        addSlide(new SlideFragmentBuilder()
                .backgroundColor(R.color.first_slide_background)
                .buttonsColor(R.color.first_slide_buttons)
                .image(R.mipmap.ic_launcher)
                .title("College Notice Board")
                        .description("Android Application for College Notice Board. +\n" +
                                "Here anyone can post their new Message in E-Board and also Tag it to given Groups. \n" +
                                "Sender only can update their Previous Message and tagged groups only can see that messages.")
                        .build());

        addSlide(new SlideFragmentBuilder()
                .backgroundColor(R.color.second_slide_background)
                .buttonsColor(R.color.second_slide_buttons)
                .image(R.drawable.slide2)
                .title("Inclusivity")
                        .description("E-Board can be used by all categories of users (HODs, Lecturers, Students...)")
                        .build());

        addSlide(new SlideFragmentBuilder()
                .backgroundColor(R.color.third_slide_background)
                .buttonsColor(R.color.third_slide_buttons)
                .image(R.drawable.slise3)
                        .title("Novice")
                        .description("E-Board allows you to to get real-time updates on the news letter")
                        .build());

        addSlide(new SlideFragmentBuilder()
                        .backgroundColor(R.color.fourth_slide_background)
                .buttonsColor(R.color.fourth_slide_buttons)
                .image(R.drawable.ic_get_started)
                        .title("Ready")
                        .description("Get Started")
                        .build());

        getBackButtonTranslationWrapper()
                .setEnterTranslation(new IViewTranslation() {
                    @Override
                    public void translate(View view, @FloatRange(from = 0, to = 1.0) float percentage) {
                        view.setAlpha(percentage);
                    }
                });

    }

    @Override
    public void onFinish() {
        super.onFinish();
        launchLoginActivity();
    }

    private void launchLoginActivity() {
        //        Initialize Tutorial Status
        Prefs.putBoolean(FIRST_RUN, false);

        startActivity(new Intent(IntroActivity.this, LoginActivity.class));
        finish();
    }
}
