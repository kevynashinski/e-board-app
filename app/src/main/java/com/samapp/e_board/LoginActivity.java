package com.samapp.e_board;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText txtEmail;
    private EditText txtPassword;
    private CoordinatorLayout loginCoordinatorLayout;
//    private Call<UserResponse> loginCall;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        if(Prefs.getBoolean(USER_LOGGED_IN,false)){
//            Toast.makeText(getBaseContext(),"Auto Authentication Success",Toast.LENGTH_SHORT).show();
//            startActivity( new Intent(LoginActivity.this,MainStudentActivity.class));
//            finish();
//        }

        assignViews();

        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email=txtEmail.getText().toString();
                String password=txtPassword.getText().toString();

if(email.isEmpty()){
    txtEmail.setError("Email Required");
    return;
}
if(password.isEmpty()){
    txtPassword.setError("Password Required");
    return;
}
                login(email,password);
            }
        });

//        findViewById(R.id.txtSignUp).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(LoginActivity.this,SignUpStudentActivity.class));
//            }
//        });

//show categories
//        MaterialDialog.Builder educationBuilder = new MaterialDialog.Builder(LoginActivity.this)
//                .customView(R.layout.dialog_categories, true)
//                .title("Select a Category")
//                .canceledOnTouchOutside(false)
//                .cancelable(false);
//
//        MaterialDialog categoryDialog = educationBuilder.build();
//        categoryDialog.show();

//        if(categoryDialog.isShowing()){
//            categoryDialog.findViewById(R.id.btnStudent).setOnClickListener(this);
//            categoryDialog.findViewById(R.id.btnLandlord).setOnClickListener(this);
//        }
    }

    private void assignViews() {
        txtEmail=(EditText)findViewById(R.id.txtEmail);
        txtPassword=(EditText)findViewById(R.id.txtPassword);
        loginCoordinatorLayout=(CoordinatorLayout)findViewById(R.id.loginCoordinatorLayout);
    }

    private void login(String email, String password){

        showProgressBar(true);

//        HostelApiInterface apiService= ApiClient.getClient().create(HostelApiInterface.class);
//        loginCall = apiService.login(email,password);
//        loginCall.enqueue(new Callback<UserResponse>() {
//            @Override
//            public void onResponse(@NonNull Call<UserResponse> call, @NonNull Response<UserResponse> response) {
//
//                showProgressBar(false);
//
//                if(response.body() != null && response.body().getMessage().equalsIgnoreCase("success")){
//                    Toast.makeText(getBaseContext(),"Login Success",Toast.LENGTH_LONG).show();
//
//                    User user=response.body().getUser().get(0);
//
////                    create session
//                    Prefs.putString(USER_ID,user.getUserId());
//                    Prefs.putString(FIRST_NAME,user.getFirstName());
//                    Prefs.putString(MIDDLE_NAME,user.getMiddleName());
//                    Prefs.putString(LAST_NAME,user.getLastName());
//                    Prefs.putString(LEVEL_ID,user.getLevelId());
//                    Prefs.putString(LEVEL_NAME,user.getLevelName());
//                    Prefs.putString(EMAIL,user.getEmail());
//                    Prefs.putString(PHONE_NUMBER,user.getPhoneNumber());
//                    Prefs.putString(GENDER,user.getGender());
//                    Prefs.putString(PROFILE_IMAGE,user.getProfileImage());
//                    Prefs.putString(USER_STATUS,user.getStatus());
//                    Prefs.putString(DATE_CREATED,user.getDateCreated());
//                    Prefs.putBoolean(USER_LOGGED_IN,true);
//
//                   startActivity( new Intent(LoginActivity.this,MainStudentActivity.class));
//                    finish();
//                } else {
//                    Toast.makeText(getBaseContext(),"Invalid Credentials",Toast.LENGTH_LONG).show();
//                }
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<UserResponse> call, @NonNull Throwable throwable) {
//
//                showProgressBar(false);
//
//                Toast.makeText(getBaseContext(),"Network Error",Toast.LENGTH_SHORT).show();
//
//                throwable.printStackTrace();
//            }
//        });
    }

    private void showProgressBar(boolean show) {
        if(show){
            dialog = new ProgressDialog(LoginActivity.this);
            dialog.setMessage("Authenticating...");
            dialog.setCanceledOnTouchOutside(false);
//            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//                @Override
//                public void onCancel(DialogInterface dialog) {
//                    loginCall.cancel();
//                }
//            });
            dialog.show();
        }else {
            dialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
//            case R.id.btnStudent:
//                startActivity(new Intent(LoginActivity.this,MainStudentActivity.class));
//                finish();
//                break;
        }
    }
}
